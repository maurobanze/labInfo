The purpose of this app is to help journalism students practice writing 
stories. It allows students to create stories on their phones, focusing on text,
audio and video. The content is later made available on a website, through which
visitors can give feedback and serve as an audience for the students.

App screenshots: https://photos.app.goo.gl/vr5TgyGop9NTuuJ8A
