package com.maurobanze.labinfo.adapters;

import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.maurobanze.labinfo.CreateEditArticleActivity;
import com.maurobanze.labinfo.R;
import com.maurobanze.labinfo.entities.Article;

import java.util.List;

import io.realm.RealmResults;

/**
 *
 */
public class ArticleListAdapter extends RecyclerView.Adapter<ArticleListAdapter.ViewHolder> {

    private List<Article> articles;
    private Fragment fragment;

    public ArticleListAdapter(RealmResults<Article> articles, Fragment fragment) {
        this.articles = articles;
        this.fragment = fragment;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View v = null;
        if (viewType == Article.ARTICLE_TYPE_PHOTO) {

            v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_article_photo, viewGroup, false);

        } else if (viewType == Article.ARTICLE_TYPE_VIDEO){

            v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_article_video, viewGroup, false);

        } else if (viewType == Article.ARTICLE_TYPE_AUDIO){

            v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_article_audio, viewGroup, false);

        }

        return new ViewHolder(v);
    }

    /**
     *
     */
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {

        Article article = articles.get(i);
        viewHolder.textViewArticleTitle.setText(article.getTitle());
        viewHolder.textViewSecondaryText.setText(article.getDateCreated().toString());

        int articleType = article.getArticleType();

        if (article.hasMedia()) {

            Uri mediaUri = Uri.parse(article.getLocalMediaUri());
            if (articleType == Article.ARTICLE_TYPE_PHOTO) {

                Glide.with(fragment).load(mediaUri).crossFade().centerCrop().into(viewHolder.imageViewArticle);

            } else if (articleType == Article.ARTICLE_TYPE_VIDEO) {

                Glide.with(fragment).load(mediaUri).into(viewHolder.imageViewArticle);

            }
        }else{
            viewHolder.imageViewArticle.setImageDrawable(null);
        }


        viewHolder.articleId = article.getRealmId();
        viewHolder.articleType = article.getArticleType();
    }

    @Override
    public int getItemCount() {
        return articles.size();
    }

    @Override
    public int getItemViewType(int position) {

        return articles.get(position).getArticleType();
    }


    /**

     */
    public class ViewHolder extends RecyclerView.ViewHolder implements OnClickListener {

        int articleId;
        int articleType;
        ImageView imageViewArticle;
        TextView textViewArticleTitle;
        TextView textViewSecondaryText;

        public ViewHolder(final View itemView) {
            super(itemView);

            imageViewArticle = (ImageView) itemView.findViewById(R.id.imageViewArticle);
            textViewArticleTitle = (TextView) itemView.findViewById(R.id.textViewArticleTitle);
            textViewSecondaryText = (TextView) itemView.findViewById(R.id.textViewSecondaryText);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            openArticleDetailsActivity();

        }

        private void openArticleDetailsActivity() {

            Intent intent = new Intent(fragment.getActivity(), CreateEditArticleActivity.class);

            intent.putExtra(CreateEditArticleActivity.EXTRA_ARTICLE_ID, articleId);
            intent.putExtra(CreateEditArticleActivity.EXTRA_ARTICLE_TYPE, articleType);

            fragment.getActivity().startActivity(intent);
        }
    }

}
