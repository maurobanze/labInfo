package com.maurobanze.labinfo;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.UploadTask;
import com.maurobanze.labinfo.cloud.CloudApi;
import com.maurobanze.labinfo.entities.Article;
import com.maurobanze.labinfo.localDatastore.LocalDatastore;
import com.maurobanze.labinfo.utils.AudioUtilities;
import com.maurobanze.labinfo.utils.MediaPicker;

import java.io.IOException;
import java.util.Date;

import io.realm.Realm;
import io.realm.RealmChangeListener;

public class CreateEditArticleActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView imageViewPhoto;
    private ViewGroup layoutHint;

    private ViewGroup layoutPlayAudio;
    private ImageButton buttonPlayPause;
    private TextView textViewAudioPosition;
    private TextView textViewAudioDuration;
    private SeekBar seekBarAudio;
    private MediaPlayer mediaPlayer;

    private ViewGroup layoutPlayVideo;
    private ImageView imageViewVideoPreview;

    private EditText editTextTitle;
    private EditText editTextDescription;
    private TextInputLayout editTextHintImageSubtitle;

    private MediaPicker mediaPicker;

    private MaterialDialog progressDialog;

    private Article article;
    private int articleType;

    private Uri mediaUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_article);

        article = new Article();
        article.setRealmId(getIntent().getIntExtra(EXTRA_ARTICLE_ID, -1));

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(backButtonListener);

        ViewGroup layoutAddMedia = (ViewGroup) findViewById(R.id.layoutAddMedia);
        layoutHint = (ViewGroup) layoutAddMedia.findViewById(R.id.layoutHint);
        imageViewPhoto = (ImageView) layoutAddMedia.findViewById(R.id.imageViewPhoto);

        layoutPlayAudio = (ViewGroup) layoutAddMedia.findViewById(R.id.layoutPlayAudio);
        buttonPlayPause = (ImageButton) layoutPlayAudio.findViewById(R.id.buttonAudioPlayPause);
        textViewAudioPosition = (TextView) layoutPlayAudio.findViewById(R.id.textViewAudioPosition);
        textViewAudioDuration = (TextView) layoutPlayAudio.findViewById(R.id.textViewAudioDuration);
        seekBarAudio = (SeekBar) layoutPlayAudio.findViewById(R.id.seekBarAudio);

        layoutPlayVideo = (ViewGroup) layoutAddMedia.findViewById(R.id.layoutPlayVideo);
        ImageButton buttonPlayVideo = (ImageButton) layoutPlayVideo.findViewById(R.id.buttonPlayVideo);
        imageViewVideoPreview = (ImageView) layoutPlayVideo.findViewById(R.id.imageViewVideoPreview);

        editTextTitle = (EditText) findViewById(R.id.editTextArticleTitle);
        editTextDescription = (EditText) findViewById(R.id.editTextArticleDescription);
        editTextHintImageSubtitle = (TextInputLayout) findViewById(R.id.editTextHintImageSubtitle);

        Button buttonSaveArticle = (Button) findViewById(R.id.buttonPublishArticle);

        layoutAddMedia.setOnClickListener(this);
        buttonSaveArticle.setOnClickListener(this);
        buttonPlayPause.setOnClickListener(this);
        buttonPlayVideo.setOnClickListener(this);

        articleType = getIntent().getIntExtra(EXTRA_ARTICLE_TYPE, 0);
        makeHintLayoutTweaks();

        if (isCreateMode()) {


        } else if (isEditMode()) {

            getSupportActionBar().setTitle(R.string.title_activity_edit_article);
            fetchArticleFromDB(article.getRealmId());

        }

    }

    private boolean isCreateMode() {

        return article.getRealmId() == -1;
    }

    private boolean isEditMode() {

        return !(isCreateMode());
    }

    private void fetchArticleFromDB(int id) {

        LocalDatastore localDatastore = LabInfoApp.fetchDatastore(this);
        localDatastore.fetchArticle(id, articleListener);

    }

    private RealmChangeListener<Article> articleListener = new RealmChangeListener<Article>() {

        @Override
        public void onChange(Article articleRealm) {

            article = articleRealm;
            populateEditTexts();

            if (article.hasMedia()) {
                mediaUri = Uri.parse(article.getLocalMediaUri());
                hideHints();
                showMedia();
            }

            article.removeChangeListeners();
        }
    };

    private void makeHintLayoutTweaks() {

        TextView textViewHint = (TextView) layoutHint.findViewById(R.id.textViewTakePhotoHint);
        ImageView imageViewHint = (ImageView) layoutHint.findViewById(R.id.imageViewHint);

        switch (articleType) {
            case ARTICLE_TYPE_PHOTO: {

                textViewHint.setText(R.string.tip_take_photo);
                imageViewHint.setImageResource(R.drawable.ic_action_image_photo);
                editTextHintImageSubtitle.setVisibility(View.VISIBLE);
                break;
            }
            case ARTICLE_TYPE_AUDIO: {

                textViewHint.setText(R.string.tip_take_audio);
                imageViewHint.setImageResource(R.drawable.ic_action_av_mic);

                break;
            }
            case ARTICLE_TYPE_VIDEO: {

                textViewHint.setText(R.string.tip_take_video);
                imageViewHint.setImageResource(R.drawable.ic_action_av_videocam);

                break;
            }
        }
    }

    private void populateEditTexts() {

        editTextTitle.setText(article.getTitle());
        editTextDescription.setText(article.getText());
    }

    /**
     * Esconde as views relacionadas com "pressione aki para tirar foto" e mostra
     * a imagem escolhida pelo utilizador.
     */
    private void hideHints() {

        layoutHint.setVisibility(View.GONE);

    }

    private void showHints() {

        layoutHint.setVisibility(View.VISIBLE);
    }

    private void showMedia() {
        showOrHideMedia(true);
    }

    private void hideMedia() {
        showOrHideMedia(false);
    }

    private void showOrHideMedia(boolean show) {

        if (articleType == ARTICLE_TYPE_PHOTO) {

            showAddedImage(show);

        } else if (articleType == ARTICLE_TYPE_AUDIO) {

            showAudioControls(show);

        } else if (articleType == ARTICLE_TYPE_VIDEO) {

            showVideoControls(show);

        }
    }

    private void showAddedImage(boolean show) {

        if (show) {

            imageViewPhoto.setVisibility(View.VISIBLE);
            Glide.with(this).load(mediaUri).centerCrop().
                    into(imageViewPhoto);
        } else {
            imageViewPhoto.setImageDrawable(null);
            imageViewPhoto.setVisibility(View.INVISIBLE);
        }

    }

    private void showAudioControls(boolean show) {

        if (show) {

            layoutPlayAudio.setVisibility(View.VISIBLE);
            prepareMediaPlayer();

        } else {
            layoutPlayAudio.setVisibility(View.INVISIBLE);
            releaseMediaPlayer();

        }
    }

    private void showVideoControls(boolean show) {

        if (show) {

            layoutPlayVideo.setVisibility(View.VISIBLE);
            Glide.with(this).load(mediaUri).into(imageViewVideoPreview);
        } else {

            layoutPlayVideo.setVisibility(View.INVISIBLE);
            imageViewVideoPreview.setImageDrawable(null);
        }

    }

    /**
     * Uses MediaPicker to show a dialog that allows users to either create or choose
     * a media file (photo, audio or video)
     */
    private void addMedia(int option) {

        mediaPicker = new MediaPicker(this);
        int mediaType = -1;

        switch (articleType) {

            case ARTICLE_TYPE_PHOTO: {

                mediaType = MediaPicker.MEDIA_TYPE_IMAGE;
                break;
            }
            case ARTICLE_TYPE_AUDIO: {

                mediaType = MediaPicker.MEDIA_TYPE_AUDIO;
                break;
            }
            case ARTICLE_TYPE_VIDEO: {

                mediaType = MediaPicker.MEDIA_TYPE_VIDEO;
                break;
            }
        }

        mediaPicker.showPickMediaDialog(mediaType, option, mediaPickerDialogListener);

    }

    private void deleteMedia() {

        mediaPicker.deleteMediaFile(mediaUri);
        mediaUri = null;

        LocalDatastore localDatastore = LabInfoApp.fetchDatastore(this);
        localDatastore.deleteArticleMediaReference(article);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {

            //determine where the result is and get it (as Uri)
            if (mediaPicker.resultReturnedInMediaPicker()) {

                mediaUri = mediaPicker.getRecordedMediaUri();

            } else if (mediaPicker.resultReturnedInIntent()) {

                try {
                    mediaUri = data.getData();
                    mediaPicker.copyToMyDirectory(data.getData());

                } catch (IOException e) {
                    e.printStackTrace();

                    Toast.makeText(this, "Impossível copiar ficheiro :(", Toast.LENGTH_SHORT).show();
                }

            }

            //change UI to hide hints and display the media properly
            hideHints();
            showMedia();

        }
    }

    private MediaPicker.DialogOptionListener mediaPickerDialogListener = new MediaPicker.DialogOptionListener() {

        @Override
        public void onDeleteMediaOptionSelected() {

            hideMedia();
            showHints();

            deleteMedia();
        }
    };


    /**
     * ////// SAVE-related OPERATIONS \\\\\\\
     */

    private boolean fieldsFilledProperly() {

        if (editTextTitle.getText().toString().isEmpty()) {

            toastWarning("Adicione um título ao artigo");
            return false;

        } else if (editTextDescription.getText().toString().isEmpty()) {

            toastWarning("Adicione um texto ao artigo");
            return false;

        } else if ((editTextHintImageSubtitle.getVisibility() == View.VISIBLE) &&
                (editTextHintImageSubtitle.getEditText().getText().toString().isEmpty())) {

            toastWarning("Adicione uma legenda à imagem");
            return false;
        }

        return true;

    }

    private void toastWarning(String msg) {

        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    private void populateOrUpdateArticleFromFields() {

        String title = editTextTitle.getText().toString();
        String text = editTextDescription.getText().toString();
        String subtitle = editTextHintImageSubtitle.getEditText().getText().toString();

        if (isCreateMode()) {

            article.setAll(title, text, subtitle, new Date(), articleType, mediaUri.toString());

        } else {//edit mode

            LocalDatastore localDatastore = LabInfoApp.fetchDatastore(this);
            localDatastore.updateArticle(article, title, text, subtitle, article.getDateCreated(), article.getArticleType(), mediaUri.toString());
        }

    }

    /**
     * Displays a progress dialog for when saving article
     */
    private void showSavingProgressDialog() {
        progressDialog = new MaterialDialog.Builder(this)
                .title("Salvando o artigo...")
                .content("Por favor aguarde")
                .progress(true, 0)
                .cancelable(true)
                .show();
    }

    private void hideProgressDialog() {

        if (progressDialog != null) {

            progressDialog.dismiss();
            progressDialog = null;
        }

    }

    private void showUploadingProgressDialog() {

        progressDialog = new MaterialDialog.Builder(this)
                .title("Enviando")
                .content("Por favor aguarde...")
                .progress(true, 100)
                .cancelable(false)
                .show();
    }

    private void saveArticleLocally() {

        LocalDatastore localDatastore = LabInfoApp.fetchDatastore(this);
        localDatastore.saveArticle(article, new Realm.Transaction.OnSuccess() {

            @Override
            public void onSuccess() {

                Toast.makeText(CreateEditArticleActivity.this, "Artigo salvo com sucesso", Toast.LENGTH_SHORT).show();

            }

        }, new Realm.Transaction.OnError() {

            @Override
            public void onError(Throwable error) {

                Toast.makeText(CreateEditArticleActivity.this, "Erro ao salvar artigo: " + error.getMessage(), Toast.LENGTH_SHORT).show();

            }

        });

    }

    private void updateArticleLocally() {

        LocalDatastore localDatastore = LabInfoApp.fetchDatastore(this);
        localDatastore.saveArticle(article, null, null);
    }

    private void deleteArticleLocally() {

        LocalDatastore localDatastore = LabInfoApp.fetchDatastore(this);
        localDatastore.deleteArticle(article);
    }

    //*** UPLOAD ARTICLE MEDIA LISTENERS  ***\\

    public OnSuccessListener<UploadTask.TaskSnapshot> uploadSuccesslistener = new OnSuccessListener<UploadTask.TaskSnapshot>() {

        @Override
        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

            Toast.makeText(CreateEditArticleActivity.this, "Artigo publicado com sucesso", Toast.LENGTH_SHORT).show();
            hideProgressDialog();

            CloudApi.updateMediaUrl(article.getId(), taskSnapshot.getMetadata().getDownloadUrl().toString());

            //TODO: decide between url or filename as media reference
            String downloadUrl = taskSnapshot.getDownloadUrl().toString();

            Toast.makeText(CreateEditArticleActivity.this, downloadUrl, Toast.LENGTH_SHORT).show();
            article.setServerMediaUrl(downloadUrl);
            article.setPublished(true);

            updateArticleLocally();
        }
    };

    public OnFailureListener uploadFailureListener = new OnFailureListener() {

        @Override
        public void onFailure(@NonNull Exception e) {

            Toast.makeText(CreateEditArticleActivity.this, "Upload failed :(", Toast.LENGTH_SHORT).show();
            hideProgressDialog();
        }
    };

    public OnProgressListener<UploadTask.TaskSnapshot> uploadProgressListener = new OnProgressListener<UploadTask.TaskSnapshot>() {

        @Override
        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {

            int progressPercent = (int) (100 * (taskSnapshot.getBytesTransferred() / taskSnapshot.getTotalByteCount()));
            //Toast.makeText(CreateEditArticleActivity.this, progressPercent + "%", Toast.LENGTH_SHORT).show();


            //int progressIncrement = progressPercent - previousProgress;
            //progressDialog.setProgress(progressPercent);
            // previousProgress = progressPercent;
        }
    };

    @Override
    public void onClick(View view) {

        int id = view.getId();
        if (id == R.id.layoutAddMedia) {

            if (isCreateMode() || !article.hasMedia()) {
                addMedia(MediaPicker.OPTION_CREATE);
            } else if (article.hasMedia()) {
                addMedia(MediaPicker.OPTION_MODIFY_OR_DELETE);
            }


        } else if (id == R.id.buttonPublishArticle) {

            if (fieldsFilledProperly()) {

                if (isCreateMode()) {

                    populateOrUpdateArticleFromFields();
                    saveArticleLocally();

                } else {
                    populateOrUpdateArticleFromFields();
                }

                CloudApi.publishArticle(article);

                if (mediaUri != null) {// ha media para se fazer upload

                    showUploadingProgressDialog();
                    CloudApi.uploadMedia(article.getId(), mediaUri, uploadSuccesslistener,
                            uploadFailureListener, uploadProgressListener);
                }
            }

        } else if (id == R.id.buttonAudioPlayPause) {

            switchPlayPause();

        } else if (id == R.id.buttonPlayVideo) {

            startPlayVideoActivity();
        }
    }

    private View.OnClickListener backButtonListener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {

            finish();
        }
    };

    private void startPlayVideoActivity() {

        Intent intent = new Intent(android.content.Intent.ACTION_VIEW);
        intent.setDataAndType(mediaUri, "video/mp4");
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_new_article, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        if (isCreateMode()) {
            menu.findItem(R.id.action_delete_article).setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_save_article) {

            if (fieldsFilledProperly()) {

                populateOrUpdateArticleFromFields();
                if (isCreateMode()) {
                    saveArticleLocally();
                }

            }

            return true;

        } else if (id == R.id.action_delete_article) {

            if (mediaUri != null)
                deleteMedia();
            deleteArticleLocally();
            finish();

        }

        return super.onOptionsItemSelected(item);
    }


    //*** AUDIO PLAYBACK ***\\

    private void prepareMediaPlayer() {

        mediaPlayer = MediaPlayer.create(this, mediaUri);
        int audioDuration = mediaPlayer.getDuration();

        textViewAudioDuration.setText(AudioUtilities.milliSecondsToTimer(audioDuration));
        textViewAudioPosition.setText("00:00");
        seekBarAudio.setMax(audioDuration);
        seekBarAudio.setProgress(0);
        seekBarAudio.setOnSeekBarChangeListener(seekBarListener);

        mediaPlayer.setOnCompletionListener(audioCompleteListener);
    }

    private void releaseMediaPlayer() {

        mediaPlayer.release();
        mediaPlayer = null;
    }

    private void switchPlayPause() {

        if (mediaPlayer.isPlaying()) {

            buttonPlayPause.setImageResource(R.drawable.ic_action_av_play_circle_fill);
            mediaPlayer.pause();

        } else {

            buttonPlayPause.setImageResource(R.drawable.ic_av_pause_circle_fill);
            mediaPlayer.start();
            myHandler.postDelayed(updateSongTime, 100);
        }
    }

    private SeekBar.OnSeekBarChangeListener seekBarListener = new SeekBar.OnSeekBarChangeListener() {

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            if (fromUser) {
                mediaPlayer.seekTo(progress);

            }
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    };

    private Handler myHandler = new Handler();

    private Runnable updateSongTime = new Runnable() {
        public void run() {

            try {

                int currentPosition = mediaPlayer.getCurrentPosition();

                seekBarAudio.setProgress(currentPosition);
                textViewAudioPosition.setText(AudioUtilities.milliSecondsToTimer(currentPosition));

                myHandler.postDelayed(this, 100);

            } catch (IllegalStateException ex) {

                //mediaPlayer has already been released in onDestroy, so mediaPlayer.getCurrentPosition()
                // can't be called. This is a concurrency issue and this try-catch solves it.
            }

        }
    };

    MediaPlayer.OnCompletionListener audioCompleteListener = new MediaPlayer.OnCompletionListener() {

        @Override
        public void onCompletion(MediaPlayer mediaPlayer) {

            buttonPlayPause.setImageResource(R.drawable.ic_action_av_play_circle_fill);
            mediaPlayer.seekTo(0);
            Toast.makeText(CreateEditArticleActivity.this, "Audio played and completed!", Toast.LENGTH_LONG).show();

        }
    };


    @Override
    protected void onPause() {
        super.onPause();

        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            mediaPlayer.pause();

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mediaPlayer != null) {

            if (mediaPlayer.isPlaying())
                mediaPlayer.stop();
            mediaPlayer.release();
        }

    }

    public static final String EXTRA_ARTICLE_ID = "ARTICLE_ID";

    public static final String EXTRA_ARTICLE_TYPE = "ARTICLE_TYPE";

    public static final int ARTICLE_TYPE_PHOTO = Article.ARTICLE_TYPE_PHOTO;
    public static final int ARTICLE_TYPE_AUDIO = Article.ARTICLE_TYPE_AUDIO;
    public static final int ARTICLE_TYPE_VIDEO = Article.ARTICLE_TYPE_VIDEO;

}
