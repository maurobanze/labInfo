package com.maurobanze.labinfo;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    private EditText editTextNrEstudante, editTextPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        editTextNrEstudante = (EditText) findViewById(R.id.editTextNrEstudante);
        editTextPassword = (EditText) findViewById(R.id.editTextPassword);

        Button buttonEntrar = (Button) findViewById(R.id.buttonLogin);
        buttonEntrar.setOnClickListener(listener);

    }

    private View.OnClickListener listener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {

            String username = editTextNrEstudante.getText().toString();
            String password = editTextPassword.getText().toString();

            if(shouldLogin(username,password)){
                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }else {
                Toast.makeText(LoginActivity.this, "Credenciais invalidas. Tente novamente", Toast.LENGTH_LONG).show();
            }

        }
    };

    private boolean shouldLogin (String username, String password){

        return username.equals("2016") && password.equals("admin");
    }
}
