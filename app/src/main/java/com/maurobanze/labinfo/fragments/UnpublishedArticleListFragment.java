package com.maurobanze.labinfo.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.maurobanze.labinfo.LabInfoApp;
import com.maurobanze.labinfo.R;
import com.maurobanze.labinfo.adapters.ArticleListAdapter;
import com.maurobanze.labinfo.entities.Article;
import com.maurobanze.labinfo.localDatastore.LocalDatastore;

import io.realm.RealmChangeListener;
import io.realm.RealmResults;

/**
 * A simple {@link Fragment} subclass.
 */
public class UnpublishedArticleListFragment extends Fragment {


    private RecyclerView recyclerView;

    public UnpublishedArticleListFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_article_list, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

        fetchArticles();

        return view;
    }

    private void fetchArticles() {

        LocalDatastore localDatastore = LabInfoApp.fetchDatastore(getActivity());
        localDatastore.fetchUnpublishedArticles(new RealmChangeListener<RealmResults<Article>>() {

            @Override
            public void onChange(RealmResults<Article> elements) {

                recyclerView.setAdapter(new ArticleListAdapter(elements, UnpublishedArticleListFragment.this));

            }
        });
    }

}
