package com.maurobanze.labinfo.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.maurobanze.labinfo.CreateEditArticleActivity;
import com.maurobanze.labinfo.R;
import com.maurobanze.labinfo.entities.Article;


public class CreateArticleOptionsFragment extends Fragment implements View.OnClickListener {


    public CreateArticleOptionsFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_create_article, container, false);

        v.findViewById(R.id.layoutNewArticlePhoto).setOnClickListener(this);
        v.findViewById(R.id.layoutNewArticleAudio).setOnClickListener(this);
        v.findViewById(R.id.layoutNewArticleVideo).setOnClickListener(this);

        return v;
    }

    @Override
    public void onClick(View view) {

        Intent intent = new Intent(getActivity(), CreateEditArticleActivity.class);

        switch (view.getId()) {
            case R.id.layoutNewArticlePhoto: {

                intent.putExtra(CreateEditArticleActivity.EXTRA_ARTICLE_TYPE, Article.ARTICLE_TYPE_PHOTO);
                break;
            }
            case R.id.layoutNewArticleAudio: {

                intent.putExtra(CreateEditArticleActivity.EXTRA_ARTICLE_TYPE, Article.ARTICLE_TYPE_AUDIO);
                break;
            }
            case R.id.layoutNewArticleVideo: {

                intent.putExtra(CreateEditArticleActivity.EXTRA_ARTICLE_TYPE, Article.ARTICLE_TYPE_VIDEO);
                break;
            }
        }
        startActivity(intent);
    }
}
