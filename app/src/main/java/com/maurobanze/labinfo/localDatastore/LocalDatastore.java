package com.maurobanze.labinfo.localDatastore;

import android.content.Context;
import android.widget.Toast;

import com.maurobanze.labinfo.entities.Article;

import java.util.Date;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmConfiguration;
import io.realm.RealmModel;
import io.realm.RealmQuery;
import io.realm.RealmResults;

/**
 *
 */
public class LocalDatastore {

    private Context context;

    public LocalDatastore(Context context) {

        this.context = context;
    }

    public void init() {

        RealmConfiguration realmConfig = new RealmConfiguration.Builder(context).build();
        Realm.setDefaultConfiguration(realmConfig);

    }

    public void saveArticle(final Article article, Realm.Transaction.OnSuccess onSuccess,
                            Realm.Transaction.OnError onError) {

        if (article.getRealmId() <= 0)
            article.setRealmId(generateNewArticleKey());

        Realm realm = Realm.getDefaultInstance();

        realm.executeTransactionAsync(new Realm.Transaction() {

            @Override
            public void execute(Realm realm) {

                realm.copyToRealmOrUpdate(article);
            }
        }, onSuccess, onError);

    }

    public void fetchUnpublishedArticles(RealmChangeListener<RealmResults<Article>> listener) {

        Realm realm = Realm.getDefaultInstance();
        RealmQuery<Article> query = realm.where(Article.class).
            equalTo("published", false);

        RealmResults<Article> results = query.findAllAsync();
        results.addChangeListener(listener);
    }

    public void fetchPublishedArticles(RealmChangeListener<RealmResults<Article>> listener) {

        Realm realm = Realm.getDefaultInstance();
        RealmQuery<Article> query = realm.where(Article.class)
                .equalTo("published", true);

        RealmResults<Article> results = query.findAllAsync();
        results.addChangeListener(listener);
    }

    public void fetchArticle(int articleId, RealmChangeListener<Article> listener){

        Realm realm = Realm.getDefaultInstance();
        RealmQuery<Article> query = realm.where(Article.class)
                .equalTo("realmId", articleId);

        Article article = query.findFirstAsync();
        article.addChangeListener(listener);

    }

    public void deleteArticleMediaReference(final Article article){

        Realm realm = Realm.getDefaultInstance();

        realm.executeTransaction(new Realm.Transaction() {

            @Override
            public void execute(Realm realm) {

                article.setLocalMediaUri(null);
            }
        });
    }

    public void updateArticleMedia(final Article article, final String uriString){
        Realm realm = Realm.getDefaultInstance();

        realm.executeTransaction(new Realm.Transaction() {

            @Override
            public void execute(Realm realm) {

                article.setLocalMediaUri(uriString);
            }
        });
    }

    public void deleteArticle(final Article article){

        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {

            @Override
            public void execute(Realm realm) {

                article.deleteFromRealm();
            }
        });
    }

    public void updateArticle(final Article article, final String title, final String text, final String mediaSubtitle,
                              final Date dateCreated, final int articleType, final String localMediaUri){

        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {

            @Override
            public void execute(Realm realm) {

                article.setAll(title,text,mediaSubtitle,dateCreated,articleType,localMediaUri);
            }
        });

    }


    private int generateNewArticleKey() {

        Realm realm = Realm.getDefaultInstance();

        int key;
        try {
            key = realm.where(Article.class).max("realmId").intValue() + 1;

        } catch (Exception ex) {
            key = 0;
        }
        return key;
    }
}
