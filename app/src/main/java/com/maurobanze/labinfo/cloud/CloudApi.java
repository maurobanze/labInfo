package com.maurobanze.labinfo.cloud;

import android.graphics.Bitmap;
import android.net.Uri;
import android.util.Log;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.maurobanze.labinfo.entities.Article;

import java.io.ByteArrayOutputStream;
import java.util.Date;

/**
 *
 */
public class CloudApi {

    private static final String FIREBASE_STORAGE_URL = "gs://labinfo-85759.appspot.com";

    public static void publishArticle(Article article) {

        //TODO: Make this dynamic
        article.setAuthorName("Mauro CJ Banze");
        article.setAuthorSchoolYear("4o");

        Log.v("firebase", "attempting save");

        DatabaseReference database = FirebaseDatabase.getInstance().getReference();

        String articleKey = database.child("articles").push().getKey();
        database.child("articles").child(articleKey).setValue(article);

        database.child("articles").child(articleKey).child("articleId").setValue(articleKey);
        Log.v("firebase", "should be saved. push_id=" + articleKey);
        article.setId(articleKey);

        //dates
    }

    public static void uploadImage(String realmArticleId, Bitmap bitmap, OnSuccessListener<UploadTask.TaskSnapshot> success,
                            OnFailureListener failure, OnProgressListener<UploadTask.TaskSnapshot> progress) {

        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageReference = storage.getReferenceFromUrl(FIREBASE_STORAGE_URL);

        StorageReference imageFileReference = storageReference.child("articles/" + realmArticleId + ".png");

        byte[] data = fromBitmapToByteArray(bitmap);

        UploadTask uploadTask = imageFileReference.putBytes(data);
        uploadTask.addOnSuccessListener(success);
        uploadTask.addOnFailureListener(failure);
        uploadTask.addOnProgressListener(progress);


    }

    public static void uploadMedia(String articleId, Uri mediaUri,  OnSuccessListener<UploadTask.TaskSnapshot> success,
                            OnFailureListener failure, OnProgressListener<UploadTask.TaskSnapshot> progress){

        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageReference = storage.getReferenceFromUrl(FIREBASE_STORAGE_URL);

        StorageReference mediaFileReference = storageReference.child("articles/" + mediaUri.getLastPathSegment());

        UploadTask uploadTask = mediaFileReference.putFile(mediaUri);
        uploadTask.addOnSuccessListener(success);
        uploadTask.addOnFailureListener(failure);
        uploadTask.addOnProgressListener(progress);

    }
    public static void updateMediaUrl(String articleId, String filename){

        DatabaseReference database = FirebaseDatabase.getInstance().getReference();
        database.child("articles").child(articleId).child("serverMediaUrl").setValue(filename);

    }

    private static byte[] fromBitmapToByteArray(Bitmap bitmap) {

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 80, baos);
        return baos.toByteArray();
    }
}
