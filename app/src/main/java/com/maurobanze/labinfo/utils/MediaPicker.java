package com.maurobanze.labinfo.utils;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.maurobanze.labinfo.R;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.Calendar;

/**
 * Created by Mauro Banze
 * This class encapsulates the functionality needed to allow the user to take a record new media or
 * choose a media file from existing files. It was designed to be used with an Activity.
 * <p>
 * This class doesn't return the actual files. It returns Uri's instead that can be used to fetch
 * the actual files (image, videos, audio)
 * <p>
 * It's recommended to use Glide Image manipulation library together with this class to easily
 * fetch the image (or video preview) from the Uri with automatic Exif rotation compensation and other very useful
 * features.
 */
public class MediaPicker {

    /**
     * The activity that this component will exist on.
     * The activity that will be called to return the image fetched by this component
     */
    private Activity activity;

    public static final int OPTION_CREATE = 1;
    public static final int OPTION_MODIFY_OR_DELETE = 2;
    public static final int REQUEST_DELETE_MEDIA = 6;

    /**
     * Constants used to store the option the user chooses (Take picture or select existing)
     */
    public static final int REQUEST_TAKE_PICTURE = 0;
    public static final int REQUEST_RECORD_VIDEO = 1;
    public static final int REQUEST_RECORD_AUDIO = 2;

    public static final int REQUEST_PICK_IMAGE = 3;
    public static final int REQUEST_PICK_VIDEO = 4;
    public static final int REQUEST_PICK_AUDIO = 5;

    private int requestMade;

    /**
     * Constants used to indicate which media type the user should be given options about
     */
    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;
    public static final int MEDIA_TYPE_AUDIO = 3;

    /**
     * The width and height of the image.
     * We should later discuss what the best size is taking quality and bandwidth usage into account
     */
    private static final int IMAGE_HEIGHT = 600;
    private static final int IMAGE_WIDTH = 800;

    private static final String MY_MEDIA_FOLDER = "LabInfo";

    /**
     * Name of the last file saved by the MediaPicker.
     */
    private String temporaryFileName;


    public MediaPicker(Activity activity) {

        this.activity = activity;
    }

    /**
     * Displays the dialog which allows the user to choose between recording or picking from existing
     * media files
     *
     * @param mediaType
     */
    public void showPickMediaDialog(final int mediaType, final int createOrModifyOption,
                                   final DialogOptionListener listener) {
        MaterialDialog.Builder builder = new MaterialDialog.Builder(activity);
        builder.negativeText(R.string.cancel);

        switch (mediaType) {
            case MEDIA_TYPE_IMAGE:

                if (createOrModifyOption == OPTION_CREATE) {

                    builder.title(R.string.add_photo);
                    builder.items(R.array.choose_photo_options);

                } else {

                    builder.title(R.string.add_photo_delete);
                    builder.items(R.array.choose_photo_options_with_delete);
                }
                break;

            case MEDIA_TYPE_VIDEO:

                if (createOrModifyOption == OPTION_CREATE) {

                    builder.title(R.string.add_video);
                    builder.items(R.array.choose_video_options);

                } else {

                    builder.title(R.string.add_video_delete);
                    builder.items(R.array.choose_video_options_with_delete);
                }
                break;

            case MEDIA_TYPE_AUDIO:

                if (createOrModifyOption == OPTION_CREATE) {

                    builder.title(R.string.add_audio);
                    builder.items(R.array.choose_audio_options);

                } else {

                    builder.title(R.string.add_audio_delete);
                    builder.items(R.array.choose_audio_options_with_delete);
                }
                break;
        }

        builder.itemsCallback(new MaterialDialog.ListCallback() {

            @Override
            public void onSelection(MaterialDialog dialog, View view, int item, CharSequence text) {

                switch (mediaType) {
                    case MEDIA_TYPE_IMAGE: {

                        if (item == 0) {//Take picture

                            requestMade = REQUEST_TAKE_PICTURE;
                            startActivityTakePicture();

                        } else if (item == 1) {//Select from gallery

                            requestMade = REQUEST_PICK_IMAGE;
                            startActivityChooseImage();

                        }
                        break;
                    }
                    case MEDIA_TYPE_VIDEO: {

                        if (item == 0) {//Record Video

                            requestMade = REQUEST_RECORD_VIDEO;
                            startActivityRecordVideo();

                        } else if (item == 1) {//Select video from gallery

                            requestMade = REQUEST_PICK_VIDEO;
                            startActivityChooseVideo();
                        }
                        break;
                    }

                    case MEDIA_TYPE_AUDIO: {

                        if (item == 0) {//Record Audio

                            requestMade = REQUEST_RECORD_AUDIO;
                            startActivityRecordAudio();

                        } else if (item == 1) {//Select audio from gallery

                            requestMade = REQUEST_PICK_AUDIO;
                            startActivityChooseAudio();
                        }

                        break;
                    }
                }

                if (item == 2) {// delete media option.

                    if(listener != null )
                        listener.onDeleteMediaOptionSelected();
                }
            }
        });

        builder.onNegative(new MaterialDialog.SingleButtonCallback() {

            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                dialog.dismiss();
            }
        });

        builder.show();


    }

    private void showPickOrDeleteMedia(final int mediaType) {

        MaterialDialog.Builder builder = new MaterialDialog.Builder(activity);
        builder.negativeText(R.string.cancel);

        switch (mediaType) {
            case MEDIA_TYPE_IMAGE:

                builder.title(R.string.add_photo_delete);
                builder.items(R.array.choose_photo_options_with_delete);

                break;
            case MEDIA_TYPE_VIDEO:

                builder.title(R.string.add_video_delete);
                builder.items(R.array.choose_video_options_with_delete);

                break;
            case MEDIA_TYPE_AUDIO:

                builder.title(R.string.add_audio_delete);
                builder.items(R.array.choose_audio_options_with_delete);
                break;
        }
    }
    //****** PICTURES ********

    private void startActivityChooseImage() {

        Intent intent = new Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");

        String title = activity.getResources().getString(R.string.choose_photo_chooser_title);
        activity.startActivityForResult(
                Intent.createChooser(intent, title),
                REQUEST_PICK_IMAGE);

    }

    private void startActivityTakePicture() {

        File mediaDirectory = getOrCreateMediaDirectory();
        File pictureFile = new File(mediaDirectory, generateMediaName(MEDIA_TYPE_IMAGE));

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(pictureFile));

        activity.startActivityForResult(intent, REQUEST_TAKE_PICTURE);

    }

    //****** AUDIO ********

    private void startActivityChooseAudio() {

        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.setType("audio/3gp|audio/AMR|audio/mp3");

        String title = activity.getResources().getString(R.string.choose_audio_chooser_title);
        activity.startActivityForResult(
                Intent.createChooser(intent, title),
                REQUEST_PICK_AUDIO);
    }

    private void startActivityRecordAudio() {

        File mediaDirectory = getOrCreateMediaDirectory();
        File pictureFile = new File(mediaDirectory, generateMediaName(MEDIA_TYPE_AUDIO));

        Intent intent = new Intent(MediaStore.Audio.Media.RECORD_SOUND_ACTION);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(pictureFile));

        activity.startActivityForResult(intent, REQUEST_RECORD_AUDIO);

    }


    //****** VIDEO ********

    private void startActivityChooseVideo() {

        Intent intent = new Intent(
                Intent.ACTION_PICK,
                MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
        intent.setType("video/*");

        String title = activity.getResources().getString(R.string.choose_video_chooser_title);
        activity.startActivityForResult(
                Intent.createChooser(intent, title),
                REQUEST_PICK_VIDEO);

    }

    private void startActivityRecordVideo() {

        File mediaDirectory = getOrCreateMediaDirectory();
        File videoFile = new File(mediaDirectory, generateMediaName(MEDIA_TYPE_VIDEO));

        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(videoFile));

        activity.startActivityForResult(intent, REQUEST_RECORD_VIDEO);

    }

    //****** GENERAL ********

    /**
     * Determines the path of the chosen media file
     */
    private String getPath(Uri uri) {

        String[] projection = {MediaStore.MediaColumns.DATA};
        Cursor cursor = activity
                .managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        cursor.moveToFirst();


        return cursor.getString(column_index);
    }

    //Uri is from intent, copy to our media folder
    public void copyToMyDirectory(Uri mediaUri) throws IOException {

        String filePath = getPath(mediaUri);
        File file = new File(filePath);

        copyFile(file, getOrCreateMediaDirectory() );

    }
    /**
     * Returns the URI of the newly created file for use by the client.
     */
    public Uri getRecordedMediaUri() {


        File mediaFile = new File(getOrCreateMediaDirectory(), temporaryFileName);
        if (!mediaFile.exists()) {
            Log.e("MEDIA", "getLastMedia. File doesn't exist. Fix this");

        }
        return Uri.fromFile(mediaFile);
    }

    public boolean resultReturnedInIntent() {

        return !(resultReturnedInMediaPicker());
    }

    public boolean resultReturnedInMediaPicker() {

        return requestMade == REQUEST_TAKE_PICTURE || requestMade == REQUEST_RECORD_VIDEO;
    }

    /**
     * Creates a new directory in the default device storage for storing the media files created
     * in this project.  If the directory already exists, it just opens it. The directory name
     * can be changed by changing the MY_MEDIA_FOLDER constant.
     */
    private File getOrCreateMediaDirectory() {

        File directory = new File(Environment.getExternalStorageDirectory(), MY_MEDIA_FOLDER);

        if (!directory.exists()) {

            Log.v("MEDIA", "Directory doesn't exist. Creating it");
            boolean created = directory.mkdirs();

            if (created) {
                return directory;
            } else {
                return null;
            }
        } else {
            Log.v("MEDIA", "Directory exists. Skipping creation");

            return directory;
        }
    }

    /**
     * Generates a new name for a newly created media file using timestamp for uniqueness.
     */
    private String generateMediaName(int mediaType) {

        Calendar c = Calendar.getInstance();
        String date = fromInt(c.get(Calendar.MONTH))
                + fromInt(c.get(Calendar.DAY_OF_MONTH))
                + fromInt(c.get(Calendar.YEAR))
                + fromInt(c.get(Calendar.HOUR_OF_DAY))
                + fromInt(c.get(Calendar.MINUTE))
                + fromInt(c.get(Calendar.SECOND));

        if (mediaType == MEDIA_TYPE_IMAGE) {

            temporaryFileName = date + ".png";
            return temporaryFileName;
        }
        if (mediaType == MEDIA_TYPE_VIDEO) {

            temporaryFileName = date + ".mp4";
            return temporaryFileName;

        }
        if (mediaType == MEDIA_TYPE_AUDIO) {

            temporaryFileName = date + ".mp3";
            return temporaryFileName;

        }

        Log.v("Media Name", "failed to generate name for media file");
        return "null";
    }

    public boolean canWriteOnExternalStorage() {

        // get the state of your external storage
        String state = Environment.getExternalStorageState();

        return state.equals(Environment.MEDIA_MOUNTED);
    }


    private String fromInt(int val) {

        return String.valueOf(val);
    }

    public boolean deleteMediaFile(Uri mediaFileUri){

        String fileName = mediaFileUri.getLastPathSegment();

        File mediaDirectory = getOrCreateMediaDirectory();
        File mediaFile = new File(mediaDirectory, fileName);

        Toast.makeText(activity, mediaFile.getAbsolutePath(), Toast.LENGTH_SHORT).show();
        if (mediaFile.exists()){
            return mediaFile.delete();

        }else {
            return false;
            //throw new IllegalArgumentException("Media file doesn't exist.");
        }
    }

    private Bitmap decodeFileToBitmap(File bitmapFile) {

        Bitmap bm;
        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();

        bm = BitmapFactory.decodeFile(bitmapFile.getAbsolutePath(),
                bitmapOptions);

        bm = Bitmap.createScaledBitmap(bm, IMAGE_WIDTH, IMAGE_HEIGHT, true);
        return bm;
    }

    private void copyFile(File oldFile, File newFileDirectory) throws IOException {

        File newFile = new File(newFileDirectory, oldFile.getName());
        FileChannel outputChannel = null;
        FileChannel inputChannel = null;
        try {
            outputChannel = new FileOutputStream(newFile).getChannel();
            inputChannel = new FileInputStream(oldFile).getChannel();
            inputChannel.transferTo(0, inputChannel.size(), outputChannel);
            inputChannel.close();

        } finally {
            if (inputChannel != null) inputChannel.close();
            if (outputChannel != null) outputChannel.close();
        }

    }

    //****** NOT WORKING - TESTS ********

    /*public File savePhoto(Bitmap bmp) throws IOException {

        File imageFileFolder = new File(
                Environment.getExternalStorageDirectory(), "/Pictures/OurMoz");

        imageFileFolder.mkdir();
        FileOutputStream out = null;

        Calendar c = Calendar.getInstance();
        String date = fromInt(c.get(Calendar.MONTH))
                + fromInt(c.get(Calendar.DAY_OF_MONTH))
                + fromInt(c.get(Calendar.YEAR))
                + fromInt(c.get(Calendar.HOUR_OF_DAY))
                + fromInt(c.get(Calendar.MINUTE))
                + fromInt(c.get(Calendar.SECOND));

        File imageFileName = new File(imageFileFolder, date + ".png");

        out = new FileOutputStream(imageFileName);
        bmp.compress(Bitmap.CompressFormat.PNG, 100, out);
        out.flush();
        out.close();

        out = null;

        return imageFileName;
    }*/

    public interface DialogOptionListener {

        void onDeleteMediaOptionSelected();
    }
}
