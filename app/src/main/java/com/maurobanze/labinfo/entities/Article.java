package com.maurobanze.labinfo.entities;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;
import com.maurobanze.labinfo.utils.MediaPicker;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

/**
 *
 */
public class Article extends RealmObject {

    private String id;

    @PrimaryKey //Realm
    @Exclude //firebase
    private int realmId;
    private String title;
    private String text;
    private String mediaSubtitle;
    private Date dateCreated;
    private Date lastEdited;
    private Date datePublished;
    private int articleType;
    private String authorName;
    private String authorSchoolYear;
    private String serverMediaUrl;

    @Exclude //firebase
    private String localMediaUri;

    @Exclude //firebase
    private boolean published;

    public Article() {

    }

    public Article(String id, String title, String text, String mediaSubtitle, Date dateCreated, Date lastEdited, int articleType, String serverMediaUrl) {
        this.id = id;
        this.title = title;
        this.text = text;
        this.mediaSubtitle = mediaSubtitle;
        this.dateCreated = dateCreated;
        this.lastEdited = lastEdited;
        this.articleType = articleType;
        this.serverMediaUrl = serverMediaUrl;
    }

    public Article(String title, String text, String mediaSubtitle, Date dateCreated, int articleType) {

        this.title = title;
        this.text = text;
        this.mediaSubtitle = mediaSubtitle;
        this.dateCreated = dateCreated;
        this.articleType = articleType;
    }

    public Article(String title, String text, String mediaSubtitle, Date dateCreated, int articleType, String localMediaUri) {
        this.title = title;
        this.text = text;
        this.mediaSubtitle = mediaSubtitle;
        this.dateCreated = dateCreated;
        this.articleType = articleType;
        this.localMediaUri = localMediaUri;
    }

    public void setAll(String title, String text, String mediaSubtitle, Date dateCreated, int articleType,
                       String localMediaUri){

        this.title = title;
        this.text = text;
        this.mediaSubtitle = mediaSubtitle;
        this.dateCreated = dateCreated;
        this.articleType = articleType;
        this.localMediaUri = localMediaUri;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getRealmId() {
        return realmId;
    }

    public void setRealmId(int realmId) {
        this.realmId = realmId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getMediaSubtitle() {
        return mediaSubtitle;
    }

    public void setMediaSubtitle(String mediaSubtitle) {
        this.mediaSubtitle = mediaSubtitle;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getLastEdited() {
        return lastEdited;
    }

    public void setLastEdited(Date lastEdited) {
        this.lastEdited = lastEdited;
    }

    public Date getDatePublished() {
        return datePublished;
    }

    public void setDatePublished(Date datePublished) {
        this.datePublished = datePublished;
    }

    public int getArticleType() {
        return articleType;
    }

    public void setArticleType(int articleType) {
        this.articleType = articleType;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getAuthorSchoolYear() {
        return authorSchoolYear;
    }

    public void setAuthorSchoolYear(String authorSchoolYear) {
        this.authorSchoolYear = authorSchoolYear;
    }

    public String getServerMediaUrl() {
        return serverMediaUrl;
    }

    public void setServerMediaUrl(String serverMediaUrl) {
        this.serverMediaUrl = serverMediaUrl;
    }

    public String getLocalMediaUri() {
        return localMediaUri;
    }

    public void setLocalMediaUri(String localMediaUri) {
        this.localMediaUri = localMediaUri;
    }

    public boolean isPublished() {
        return published;
    }

    public void setPublished(boolean published) {
        this.published = published;
    }

    public boolean hasMedia(){
        return localMediaUri != null;
    }

    public static final int ARTICLE_TYPE_PHOTO = 1;
    public static final int ARTICLE_TYPE_VIDEO = 2;
    public static final int ARTICLE_TYPE_AUDIO = 3;

}
