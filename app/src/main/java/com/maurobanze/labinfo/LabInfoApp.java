package com.maurobanze.labinfo;

import android.app.Activity;
import android.app.Application;
import android.content.Context;

import com.maurobanze.labinfo.localDatastore.LocalDatastore;

/**
 *
 */
public class LabInfoApp extends Application {

    private LocalDatastore datastore;

    @Override
    public void onCreate() {
        super.onCreate();

        datastore = new LocalDatastore(this);
        datastore.init();
    }

    public LocalDatastore getDatastore() {
        return datastore;
    }

    public void setDatastore(LocalDatastore datastore) {
        this.datastore = datastore;
    }

    public static LocalDatastore fetchDatastore(Activity activity){

        LabInfoApp app = (LabInfoApp) activity.getApplication();
        LocalDatastore datastore = app.getDatastore();

        return datastore;

    }
}
